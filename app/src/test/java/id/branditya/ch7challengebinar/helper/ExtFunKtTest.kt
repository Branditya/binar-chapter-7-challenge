package id.branditya.ch7challengebinar.helper

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ExtFunKtTest {

    @Test
    fun toDate() {
        val date = "2022-12-24"
        assertEquals("Des 24, 2022", date.toDate())
    }

    @Test
    fun toDateDMY() {
        val date = "2022-12-24"
        assertEquals("24/12/2022", date.toDateDMY())
    }

    @Test
    fun toHourMinute() {
        val time = 156
        assertEquals("2h 36m", time.toHourMinute())

    }
}