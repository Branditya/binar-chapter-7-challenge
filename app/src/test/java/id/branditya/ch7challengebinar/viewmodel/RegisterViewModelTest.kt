package id.branditya.ch7challengebinar.viewmodel

import id.branditya.ch7challengebinar.helper.AccountRepo
import io.mockk.mockk
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class RegisterViewModelTest {
    lateinit var registerViewModel: RegisterViewModel
    lateinit var accountRepo: AccountRepo

    @Before
    fun setUp() {
        accountRepo = mockk()
        registerViewModel = RegisterViewModel(accountRepo)
    }

    @Test
    fun checkInput() {
        val username = "tes"
        val email = "tes@gmail.com"
        val password = "12345"
        val passwordConfirm = "12345123"
        val result = registerViewModel.checkEmptyInput(username, email, password, passwordConfirm)
        assertEquals("Password tidak sesuai", result)
    }
}