package id.branditya.ch7challengebinar.viewmodel

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import id.branditya.ch7challengebinar.helper.AccountDataStoreManager
import id.branditya.ch7challengebinar.helper.AccountRepo
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.runners.MockitoJUnitRunner

class LoginViewModelTest {

    lateinit var viewModel: LoginViewModel
    lateinit var pref: AccountDataStoreManager
    lateinit var accountRepo: AccountRepo


    @Before
    fun setUp() {
        pref = mockk()
        accountRepo = mockk()
        viewModel = LoginViewModel(pref, accountRepo)
    }

    @Test
    fun checkInputEmail() {
        val input = ""
        val result = viewModel.checkInputEmail(input)
        assertEquals("Email harus diisi", result)
    }

    @Test
    fun checkInputPassword() {
        val input = ""
        val result = viewModel.checkInputPassword(input)
        assertEquals("Password harus diisi", result)
    }

}