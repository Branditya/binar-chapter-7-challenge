package id.branditya.ch7challengebinar.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
