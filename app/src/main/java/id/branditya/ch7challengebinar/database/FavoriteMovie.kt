package id.branditya.ch7challengebinar.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteMovie(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "accountId") val accountId: Int,
    @ColumnInfo(name = "movieId") val movieId: Int
)
