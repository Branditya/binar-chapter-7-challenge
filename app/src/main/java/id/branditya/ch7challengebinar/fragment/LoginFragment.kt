package id.branditya.ch7challengebinar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.branditya.ch7challengebinar.R
import id.branditya.ch7challengebinar.databinding.FragmentLoginBinding
import id.branditya.ch7challengebinar.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvToRegisterClicked()
        btnLoginClicked()
        observeData()
    }


    private fun btnLoginClicked() {
        binding.apply {
            btnLogin.setOnClickListener {
                val etEmail = etEmail.text.toString()
                val etPassword = etPassword.text.toString()
                tilEmail.error = loginViewModel.checkInputEmail(etEmail)
                tilPassword.error = loginViewModel.checkInputPassword(etPassword)
                if (etEmail.isNotEmpty() && etPassword.isNotEmpty()) {
                    loginViewModel.loginAction(etEmail, etPassword)
                }
            }
        }
    }

    private fun observeData() {
        loginViewModel.accountRegistered.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Login Berhasil").show()
                loginToHomeFragment()
            } else {
                createToast("Email atau Password Salah").show()
            }
        }
    }

    private fun loginToHomeFragment() {
        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
    }

    private fun tvToRegisterClicked() {
        binding.apply {
            tvToRegister.setOnClickListener {
                binding.etEmail.text?.clear()
                binding.etPassword.text?.clear()
                it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}