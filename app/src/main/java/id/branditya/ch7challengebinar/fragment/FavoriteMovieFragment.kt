package id.branditya.ch7challengebinar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.branditya.ch7challengebinar.R
import id.branditya.ch7challengebinar.adapter.MovieAdapter
import id.branditya.ch7challengebinar.databinding.FragmentFavoriteMovieBinding
import id.branditya.ch7challengebinar.helper.AccountDataStoreManager
import id.branditya.ch7challengebinar.helper.MovieRepo
import id.branditya.ch7challengebinar.helper.viewModelsFactory
import id.branditya.ch7challengebinar.service.ApiClient
import id.branditya.ch7challengebinar.service.ApiService
import id.branditya.ch7challengebinar.viewmodel.HomeViewModel

//Masih belum
class FavoriteMovieFragment : Fragment() {
    private var _binding: FragmentFavoriteMovieBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter


    private val apiService: ApiService by lazy { ApiClient.instance }

    private val movieRepo: MovieRepo by lazy { MovieRepo(apiService) }
    private val pref: AccountDataStoreManager by lazy { AccountDataStoreManager(requireContext()) }

    private val viewModel: HomeViewModel by viewModelsFactory { HomeViewModel(pref, movieRepo) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoriteMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun initRecyclerView() {
        binding.apply {
            movieAdapter = MovieAdapter {
                val movieId = it.id
                val bundle = Bundle()
                bundle.putInt("KEY_ID", movieId)
                findNavController().navigate(
                    R.id.action_homeFragment_to_detailMovieFragment,
                    bundle
                )
            }
            rvFavorite.adapter = movieAdapter
            rvFavorite.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeData() {
    }
}