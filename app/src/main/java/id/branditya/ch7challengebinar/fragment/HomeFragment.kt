package id.branditya.ch7challengebinar.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.branditya.ch7challengebinar.R
import id.branditya.ch7challengebinar.adapter.MovieAdapter
import id.branditya.ch7challengebinar.databinding.FragmentHomeBinding
import id.branditya.ch7challengebinar.databinding.LayoutDialogDetailUserBinding
import id.branditya.ch7challengebinar.model.Status
import id.branditya.ch7challengebinar.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private val homeViewModel: HomeViewModel by viewModel()

    var username: String = ""
    var email: String = ""
    var accountId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        changeTvTitle()
        profileButtonClicked()
        favoriteButtonClicked()
        initRecyclerView()
        observeData()
    }

    private fun observeData() {
        homeViewModel.getDataListMovieFromNetwork().observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.pbHome.isVisible = true
                }
                Status.SUCCESS -> {
                    binding.pbHome.isVisible = false
                    it.data?.let { _ ->
                        movieAdapter.updateData(it.data)
                    }
                }
                Status.ERROR -> {
                    binding.pbHome.isVisible = false
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
        homeViewModel.getDataUsername().observe(viewLifecycleOwner) {
            username = it
        }
        homeViewModel.getDataEmail().observe(viewLifecycleOwner) {
            email = it
        }
        homeViewModel.getDataAccountId().observe(viewLifecycleOwner) {
            accountId = it
        }
    }

    private fun changeTvTitle() {
        homeViewModel.getDataUsername().observe(viewLifecycleOwner) {
            binding.tvShowUsername.text = it
        }
    }

    private fun profileButtonClicked() {
        binding.btnShowProfile.setOnClickListener {
            showDialogProfile()
        }
    }

    private fun favoriteButtonClicked() {
        binding.btnFavorite.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_favoriteMovieFragment)
        }
    }

    private fun showDialogProfile() {
        val binding = LayoutDialogDetailUserBinding.inflate(
            LayoutInflater.from(requireContext()),
            null,
            false
        )
        val customLayout = binding.root

        binding.tvProfileUsername.text = username
        binding.tvProfileEmail.text = email

        val builder = AlertDialog.Builder(requireContext())
        builder.setView(customLayout)
        val dialog = builder.create()
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        binding.apply {
            btnEditProfile.setOnClickListener {
                val bundle = Bundle()
                bundle.putInt(ACCOUNT_ID_KEY, accountId)
                findNavController().navigate(R.id.action_homeFragment_to_profileFragment, bundle)
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    private fun initRecyclerView() {
        movieAdapter = MovieAdapter {
            val movieId = it.id
            val bundle = Bundle()
            bundle.putInt(MOVIE_ID_KEY, movieId)
            bundle.putInt(ACCOUNT_ID_KEY, accountId)
            findNavController().navigate(
                R.id.action_homeFragment_to_detailMovieFragment,
                bundle
            )
        }
        binding.apply {
            rvHome.adapter = movieAdapter
            rvHome.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    companion object {
        const val MOVIE_ID_KEY = "movie_id_key"
        const val ACCOUNT_ID_KEY = "account_id_key"
    }
}