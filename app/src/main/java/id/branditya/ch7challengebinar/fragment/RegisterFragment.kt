package id.branditya.ch7challengebinar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.branditya.ch7challengebinar.databinding.FragmentRegisterBinding
import id.branditya.ch7challengebinar.viewmodel.RegisterViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var username: String = ""
    private var email: String = ""
    private var password: String = ""

    private val registerViewModel: RegisterViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkEditTextOnChange()
        registerBtnClicked()
        observeData()
    }

    private fun checkEditTextOnChange() {
        binding.apply {
            etUsername.doAfterTextChanged {
                if (etUsername.text.toString().isEmpty()) {
                    tilUsername.error = "Username harus diisi"
                } else {
                    tilUsername.error = null
                }
            }
            etEmail.doAfterTextChanged {
                if (etEmail.text.toString().isEmpty()) {
                    tilEmail.error = "Email harus diisi"
                } else {
                    tilEmail.error = null
                }
            }
            etPassword.doAfterTextChanged {
                if (etPassword.text.toString().isEmpty()) {
                    tilPassword.error = "Password harus diisi"
                } else {
                    tilPassword.error = null
                }
            }
            etPasswordConfirm.doAfterTextChanged {
                when {
                    etPasswordConfirm.text.toString().isEmpty() -> {
                        tilPasswordConfirm.error = "Password harus diisi"
                    }
                    checkConfirmPassword() -> {
                        tilPasswordConfirm.error = null
                    }
                    else -> {
                        tilPasswordConfirm.error = "Password tidak sesuai"
                    }
                }
            }
        }

    }

    private fun checkConfirmPassword(): Boolean {
        binding.apply {
            return etPassword.text.toString() == etPasswordConfirm.text.toString()
        }
    }

    private fun registerBtnClicked() {
        binding.apply {
            btnRegister.setOnClickListener {
                username = etUsername.text.toString()
                email = etEmail.text.toString()
                password = etPassword.text.toString()
                val passwordConfirm = etPasswordConfirm.text.toString()
                val result =
                    registerViewModel.checkEmptyInput(username, email, password, passwordConfirm)
                if (result == null) {
                    registerViewModel.checkRegisteredEmail(email)
                }
            }
        }
    }

    private fun observeData() {
        registerViewModel.accountRegistered.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Email Sudah Terdaftar").show()
            } else {
                registerViewModel.saveToDb(username, email, password)
            }
        }
        registerViewModel.accountAdded.observe(viewLifecycleOwner) {
            if (it == true) {
                registerToLoginFragment()
                createToast("Register Berhasil")
            }
        }
    }

    private fun registerToLoginFragment() {
        findNavController().popBackStack()
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}