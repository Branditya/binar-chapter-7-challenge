package id.branditya.ch7challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import id.branditya.ch7challengebinar.helper.AccountDataStoreManager

class SplashScreenViewModel(private val pref: AccountDataStoreManager) : ViewModel() {
    fun getLoginStatusPref(): LiveData<Boolean> {
        return pref.getLoginStatus().asLiveData()
    }
}