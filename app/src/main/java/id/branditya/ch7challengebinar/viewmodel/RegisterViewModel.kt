package id.branditya.ch7challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch7challengebinar.database.Account
import id.branditya.ch7challengebinar.helper.AccountRepo
import kotlinx.coroutines.launch

class RegisterViewModel(private val accountRepo: AccountRepo) : ViewModel() {
    private var _accountRegistered = MutableLiveData<Boolean>()
    val accountRegistered: LiveData<Boolean> get() = _accountRegistered

    private var _accountAdded = MutableLiveData<Boolean>()
    val accountAdded: LiveData<Boolean> get() = _accountAdded

    fun checkRegisteredEmail(email: String) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountWithEmail(email)
            _accountRegistered.postValue(!result.isNullOrEmpty())
        }
    }

    fun saveToDb(username: String, email: String, password: String) {
        val account = Account(null, username, email, password, username, "", "", null)
        viewModelScope.launch {
            val result = accountRepo.insertAccount(account)
            if (result != 0L) {
                _accountAdded.postValue(true)
            }
        }
    }

    fun checkEmptyInput(
        username: String,
        email: String,
        password: String,
        passwordConfirm: String
    ): String? {
        return when {
            username.isEmpty() -> {
                "Username harus diisi"
            }
            email.isEmpty() -> {
                "Email harus diisi"
            }
            password.isEmpty() -> {
                "Password harus diisi"
            }
            passwordConfirm.isEmpty() -> {
                "Password harus diisi"
            }
            password != passwordConfirm -> {
                "Password tidak sesuai"
            }
            else -> {
                null
            }
        }
    }
}