package id.branditya.ch7challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import id.branditya.ch7challengebinar.helper.AccountDataStoreManager
import id.branditya.ch7challengebinar.helper.MovieRepo
import id.branditya.ch7challengebinar.model.Resource
import kotlinx.coroutines.Dispatchers

class HomeViewModel(private val pref: AccountDataStoreManager, private val movieRepo: MovieRepo) :
    ViewModel() {

    fun getDataUsername(): LiveData<String> {
        return pref.getDataUsername().asLiveData()
    }

    fun getDataEmail(): LiveData<String> {
        return pref.getDataEmail().asLiveData()
    }

    fun getDataAccountId(): LiveData<Int> {
        return pref.getDataAccountId().asLiveData()
    }

    fun getDataListMovieFromNetwork() = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        try {
            emit(Resource.success(movieRepo.getDataListMovieFromNetwork().resultPopularMovies))
        } catch (e: Exception) {
            emit(Resource.error(data = null, message = e.message ?: "Error Occurred!"))
        }
    }

}