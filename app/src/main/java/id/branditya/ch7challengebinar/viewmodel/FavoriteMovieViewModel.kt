package id.branditya.ch7challengebinar.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch7challengebinar.helper.AccountRepo
import id.branditya.ch7challengebinar.helper.MovieRepo
import kotlinx.coroutines.launch

//Masih belum
class FavoriteMovieViewModel(
    private val movieRepo: MovieRepo,
    private val accountRepo: AccountRepo
) : ViewModel() {

    fun getData(accountId: Int) {
        viewModelScope.launch {
            val movieId = accountRepo.getFavoriteMovieList(accountId)
            if (movieId != null) {
                for (id in movieId) {

                }
            }
            movieRepo.getDataListMovieFromNetwork()
        }
    }
}