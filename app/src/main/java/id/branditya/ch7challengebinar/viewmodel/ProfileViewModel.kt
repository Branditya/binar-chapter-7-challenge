package id.branditya.ch7challengebinar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.branditya.ch7challengebinar.database.Account
import id.branditya.ch7challengebinar.helper.AccountDataStoreManager
import id.branditya.ch7challengebinar.helper.AccountRepo
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val pref: AccountDataStoreManager,
    private val accountRepo: AccountRepo
) : ViewModel() {

    private var _detailAccount = MutableLiveData<Account>()
    val detailAccount: LiveData<Account> get() = _detailAccount

    private var _updatedData = MutableLiveData<Boolean>()
    val updatedData: LiveData<Boolean> get() = _updatedData

    private var _profileImage = MutableLiveData<String?>()
    val profileImage: LiveData<String?> get() = _profileImage

    private fun setDataUsernamePrefAccount(username: String) {
        viewModelScope.launch {
            pref.setDataUsernamePrefAccount(username)
        }
    }

    fun clearDataPrefAccount() {
        viewModelScope.launch {
            pref.clearDataPrefAccount()
        }
    }

    fun getProfileData(accountId: Int) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountWithId(accountId)
            if (result != null) {
                _detailAccount.postValue(result[0])
            }
        }
    }

    fun updateData(
        accountId: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ) {
        viewModelScope.launch {
            val result =
                accountRepo.updateProfileAccount(accountId, username, fullname, birthdate, address)
            if (result != 0) {
                setDataUsernamePrefAccount(username)
                _updatedData.postValue(true)
            } else {
                _updatedData.postValue(false)
            }
        }
    }

    fun saveProfileImage(accountId: Int, profileImage: String) {
        viewModelScope.launch {
            accountRepo.updateProfileImage(accountId, profileImage)
        }
    }

    fun getProfileImage(accountId: Int) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountWithId(accountId)
            if (!result.isNullOrEmpty()) {
                _profileImage.postValue(result[0].profileImage)
            } else {
                _profileImage.postValue(accountId.toString())
            }
        }
    }

}