package id.branditya.ch7challengebinar.helper

import id.branditya.ch7challengebinar.BuildConfig
import id.branditya.ch7challengebinar.service.ApiService

class MovieRepo(private val apiService: ApiService) {

    suspend fun getDataDetailMovieFromNetwork(movieId: Int) =
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY)

    suspend fun getDataListMovieFromNetwork() = apiService.getPopularMovie(BuildConfig.API_KEY)
}